FROM composer AS composer
FROM php:8-apache

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN mkdir -p /var/www/html/public

RUN curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh

# Install dependencies as per server requirements
# https://statamic.dev/requirements#server-requirements
# View default extensions with: docker run --rm php:7.4-fpm php -m
RUN apt-get update \
  && apt-get install -y \
    libwebp-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libzip-dev \
    zip \
    nodejs \
    default-mysql-client \
  && docker-php-ext-install exif \
  && docker-php-ext-install bcmath \
  && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
  && docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql

COPY app/ /var/www/html

RUN cd /var/www/html/public

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite

COPY /installerfiles/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

CMD bash -c '/entrypoint.sh';'/bin/bash'