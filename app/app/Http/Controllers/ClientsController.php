<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    
    public function index()
    {
        //Utilizamos la funcion del modelo para hacer un query a la base de datos y seleccionar todos los registros
        $clients = Client::all();
        //Pasamos los registros a la vista para que sean desplegados en la lista de clientes
        return view('clients.index', ['clients' => $clients]);
    }
    
    public function create(Request $request)
    {
        //Creamos una nueva instancia del modelo
        $client = new Client;
        //Llenamos sus atributos con la información que proviene del formulario
        $client->name = $request->input('name');
        $client->lastname = $request->input('lastname');
        $client->email = $request->input('email');
        $client->phone = $request->input('phone');
        //Invocamos el metodo save para pesistir la información a la vase de datos
        $client->save();
        //Redirigimos al usario a la pagina de clientes
        return redirect()->route('clientes');
    }
    
    public function show($id)
    {
        //Utilizamos el modelo para localizar el registro del cliente en la base de datos
        $client = Client::find($id);
        //Con la información del cliente y aprovechando la relación definida en la base de datos, seleccionamos todas las ordenes de este cliente
        $orders = $client->orders;
        //Respondemos con la vista de detalle del cliente, le pasamos la información que solicitamos a la base de datos
        return view('clients.show',[ 'client' => $client, 'orders' => $orders ]);
    }

}