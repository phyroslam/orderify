<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Order;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    
    public function index()
    {
        
        $clients = Client::limit(5)
            ->orderByDesc('created_at')
            ->get();
        $orders = Order::limit(10)
            ->orderByDesc('created_at')
            ->get();
        //Pasamos los registros a la vista para que sean desplegados en la lista de clientes
        return view('index', ['clients' => $clients, 'orders' => $orders]);
    }
    

}