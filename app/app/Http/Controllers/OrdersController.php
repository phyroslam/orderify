<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Client;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    
    public function index()
    {
        //Necesitamos todos los clientes para poder selecionarlos a la hora de crear una orden
        $clients = Client::all();
        //Cagamos las ordenes existentes en el sistema
        $orders = Order::with('client')->get();
        //Pasamos los registros a la vista para que sean desplegados en la lista de orders
        return view('orders.index', ['orders' => $orders, 'clients' => $clients]);
    }
    
    public function create(Request $request)
    {
        $client = Client::findOrFail($request->input('client_id'));
        //Creamos una nueva instancia del modelo
        $order = new order;
        //Llenamos sus atributos con la información que proviene del formulario
        $order->client_id = $client->id;
        $order->product = $request->input('product');
        $order->amount = $request->input('amount');
        $order->street = $request->input('street');
        $order->street_number = $request->input('street_number');
        $order->internal_number = $request->input('internal_number');
        $order->postal_code = $request->input('postal_code');
        $order->state = $request->input('state');
        //Invocamos el metodo save para pesistir la información a la vase de datos
        $order->save();
        //Redirigimos al usario a la pagina de orders
        return redirect()->route('ordenes');
    }
    
    public function show($id)
    {
        //Utilizamos el modelo para localizar el registro del ordere en la base de datos
        $order = Order::find($id);
        //Con la información del ordere y aprovechando la relación definida en la base de datos, seleccionamos todas las ordenes de este ordere
        $orders = $order->orders();
        //Respondemos con la vista de detalle del ordere, le pasamos la información que solicitamos a la base de datos
        return view('orders.show',[ 'order' => $order, 'orders' => $orders ]);
    }

}