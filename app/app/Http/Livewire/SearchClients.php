<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Client;

class SearchClients extends Component
{

    public $search = '';

    public function render()
    {
        return view('livewire.search-clients',[
            'clients' => Client::where('name', 'like', '%' . $this->search . '%')->get()
        ]);
    }
}
