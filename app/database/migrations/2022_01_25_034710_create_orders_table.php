<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('client_id');
            $table->string('street', 200)->nullable();
            $table->string('street_number', 10)->nullable();
            $table->string('internal_number', 10)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->string('state', 30)->nullable();
            $table->string('product', 10)->nullable();
            $table->decimal('amount', $precision = 8, $scale = 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
