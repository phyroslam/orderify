@extends('layout')


@section('content')

<h1 class="h2">Clientes</h1>
<p class="mt-3">Listado y creación de clientes de Ordify</p>

<hr class="my-5">

<h1 class="h5">Agregar nuevo cliente</h1>
<p class="mt-3">Ingresa los datos y guarda la información</p>

<section class="bg-gray-100">
  <div class="max-w-screen-xl px-4 py-8 mx-auto sm:px-6 lg:px-8">
    <div class="grid grid-cols-1 gap-x-16 gap-y-8 lg:grid-cols-5">
      <div class="p-8 bg-white rounded-lg shadow-lg lg:p-12 lg:col-span-3">
        <form action="{{ route('clientes.crear') }}" method="POST" class="space-y-4">
          @csrf
          <div>
            <label class="sr-only" for="name">Nombre</label>
            <input class="w-full p-3 text-sm border border-gray-200 rounded-lg" placeholder="Nombre" type="text" id="name" name="name" />
          </div>

          <div>
            <label class="sr-only" for="name">Apellido</label>
            <input class="w-full p-3 text-sm border border-gray-200 rounded-lg" placeholder="Apellido" type="text" id="lastname" name="lastname" />
          </div>

          <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
            <div>
              <label class="sr-only" for="email">Email</label>
              <input
                class="w-full p-3 text-sm border border-gray-200 rounded-lg"
                placeholder="Email"
                type="email"
                id="email"
                name="email"
              />
            </div>

            <div>
              <label class="sr-only" for="phone">Telefono</label>
              <input
                class="w-full p-3 text-sm border border-gray-200 rounded-lg"
                placeholder="Telefono"
                type="tel"
                id="phone"
                name="phone"
              />
            </div>
          </div>

          <div class="mt-4">
            <button
              type="submit"
              class="inline-flex items-center justify-center w-full px-5 py-3 text-white bg-black rounded-lg sm:w-auto"
            >
              <span class="font-medium"> Agregar cliente </span>

              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="w-5 h-5 ml-3"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
              </svg>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>


<hr class="my-5">

<h1 class="h5">Listado de clientes</h1>
<p class="mt-3">Clientes en el sistema</p>

@livewire('search-clients')

@endsection