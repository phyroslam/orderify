<!-- Clients -->
<div class="card">
    
    <div class="card-header">Clientes</div>

    @foreach ($clients as $client) 
    <div class="p-6 flex flex-row justify-between items-center text-gray-600 border-b">
        <div class="flex items-center"> 
            <a href="{{ route('cliente', ['id' => $client->id ]); }}"><h1>{{ $client->name }} {{ $client->lastname }}</h1></a>
        </div> 
    </div>
    @endforeach

</div>
<!-- end Clients -->