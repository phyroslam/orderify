@extends('layout')

@section('content')

<h1 class="h2">{{ $client->name }} {{ $client->lastname}}</h1>
<p class="mt-3 h6">{{ $client->email }} - {{ $client->phone }}</p>

<hr class="my-5">

<h3 class="h5">Ordenes</h3>
<p class="mt-3">Ordenes realizadas</p>

@include('orders.partials.orderstable')

@endsection