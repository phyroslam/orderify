@extends('layout')

@section('content')

<!-- congrats & summary -->
<div class="grid grid-cols-3 lg:grid-cols-1 gap-5">
    <!-- congrats -->
    <div class="card col-span-1">

        <div class="card-body h-full flex flex-col justify-between">

            <div>
                <h1 class="text-lg font-bold tracking-wide">Bienvenido a Orderify</h1>
                <p class="text-gray-600 mt-2">Las mejores ordenes en internet</p>
            </div>
        
            <div class="flex flex-row mt-10 items-end">
        
                <div class="flex-1">
                    <h1 class="font-extrabold text-4xl text-teal-400">$89k</h1>
                    <p class="mt-3 mb-4 text-xs text-gray-500">Total de ordenes</p>
                    <a href="{{ route('ordenes') }}" class="btn-shadow py-3">
                        Ver Ordenes
                    </a>
                </div>
        
                <div class="flex-1 ml-10 w-32 h-32 lg:w-auto lg:h-auto overflow-hidden">
                    <img class="object-cover" src="img/congrats.svg">
                </div>
        
            </div>

        </div>
        
    </div>
    <!-- end congrats -->
</div>

<!-- start quick Info -->
<div class="grid grid-cols-3 gap-6 mt-6 xl:grid-cols-1">


@livewire('search-clients')

<!-- Start Recent Sales -->
<div class="col-span-2 xl:col-span-1">
@include('orders.partials.orderstable')
</div>
<!-- End Recent Sales -->


</div>
<!-- end quick Info -->
@endsection