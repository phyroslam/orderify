<div>
    
    <div class="card">
    
        <div class="card-header">Clientes</div>

        <div class="p-6 mx-auto bg-white border-b">
            <form class="flex mb-0 space-x-4">
                <div class="flex-1">
                    <label class="sr-only" for="search"> Name </label>

                    <input
                        class="w-full h-12 text-sm border border-gray-300 rounded-lg p-2"
                        id="search"
                        placeholder="Search"
                        wire:model="search"
                    />
                </div>
            </form> 
        </div>

        @foreach ($clients as $client) 
        <div class="p-6 flex flex-row justify-between items-center text-gray-600 border-b">
            <div class="flex items-center"> 
                <a href="{{ route('cliente', ['id' => $client->id ]); }}"><h1>{{ $client->name }} {{ $client->lastname }}</h1></a>
            </div> 
        </div>
        @endforeach

    </div>

</div>
