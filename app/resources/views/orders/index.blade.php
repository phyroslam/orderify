@extends('layout')


@section('content')

<h1 class="h2">Ordenes</h1>
<p class="mt-3">Listado y creación ordenes</p>

<hr class="my-5">

<h1 class="h5">Agregar nueva orden</h1>
<p class="mt-3">Ingresa los datos y guarda la información</p>

<section class="bg-gray-100">
  <div class="max-w-screen-xl px-4 py-8 mx-auto sm:px-6 lg:px-8">
    <div class="grid grid-cols-1 gap-x-16 gap-y-8 lg:grid-cols-5">
      <div class="p-8 bg-white rounded-lg shadow-lg lg:p-12 lg:col-span-3">
        <form action="{{ route('ordenes.crear') }}" method="POST" class="space-y-4">
          @csrf
          <div>
            <label class="sr-only" for="name">Cliente</label>
            <select class="w-full h-12 text-sm border border-gray-100 rounded-lg" id="type" name="client_id">
              @foreach ($clients as $client)
              <option value="{{ $client->id }}">{{ $client->name }} {{  $client->lastname }}</option>
              @endforeach
            </select>
          </div>


          <div>
            <label class="sr-only" for="name">Producto</label>
            <select class="w-full h-12 text-sm border border-gray-100 rounded-lg" id="type" name="product">
              <option>Abrigo</option>
              <option>Zapatos</option>
              <option>Pantalon</option>
              <option>Camisa</option>
              <option>Bolso</option>
            </select>
          </div>

          <div>
            <label class="sr-only" for="name">Precio</label>
            <input class="w-full p-3 text-sm border border-gray-200 rounded-lg" placeholder="Precio" type="text" id="amount" name="amount" />
          </div>

          <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
            <div>
              <label class="sr-only" for="email">Calle</label>
              <input
                class="w-full p-3 text-sm border border-gray-200 rounded-lg"
                placeholder="Calle"
                type="text"
                id="street"
                name="street"
              />
            </div>

            <div>
              <label class="sr-only" for="house_number">Numero exterior</label>
              <input
                class="w-full p-3 text-sm border border-gray-200 rounded-lg"
                placeholder="Numero Exterior"
                type="text"
                id="street_number"
                name="street_number"
              />
            </div>

            <div>
              <label class="sr-only" for="house_number">Numero interior</label>
              <input
                class="w-full p-3 text-sm border border-gray-200 rounded-lg"
                placeholder="Numero Interior"
                type="text"
                id="internal_number"
                name="internal_number"
              />
            </div>

            <div>
              <label class="sr-only" for="house_number">Codigo Postal</label>
              <input
                class="w-full p-3 text-sm border border-gray-200 rounded-lg"
                placeholder="Codigo Postal"
                type="text"
                id="postal_code"
                name="postal_code"
              />
            </div>

            <div>
              <label class="sr-only" for="name">Estado</label>
              <select class="w-full h-12 text-sm border border-gray-100 rounded-lg" id="type" name="state">
                <option>Aguas Calientes</option>
                <option>Sinaloa</option>
                <option>Jalisco</option>
                <option>Ciudad de México</option>
                <option>Campeche</option>
              </select>
            </div>

          </div>

          <div class="mt-4">
            <button
              type="submit"
              class="inline-flex items-center justify-center w-full px-5 py-3 text-white bg-black rounded-lg sm:w-auto"
            >
              <span class="font-medium"> Agregar orden </span>

              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="w-5 h-5 ml-3"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
              </svg>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>


<hr class="my-5">

<h1 class="h5">Listado de ordenes</h1>
<p class="mt-3">Ordenes en el sistema</p>

@include('orders.partials.orderstable')

@endsection