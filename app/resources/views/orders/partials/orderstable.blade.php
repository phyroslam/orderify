<!-- Ordenes -->
<div class="card">
    
    <div class="card-header">Ordenes</div>

    <table class="table-auto w-full text-left">
        <thead>
            <tr>
                <th class="px-4 py-2 border-r"></th>
                <th class="px-4 py-2 border-r">producto</th>
                <th class="px-4 py-2 border-r">precio</th>
                <th class="px-4 py-2">cliente</th>
            </tr>
        </thead>
        <tbody class="text-gray-600">

        @foreach ($orders as $order) 
            <tr>                    
                <td class="border border-l-0 border-b-0 px-4 py-2 text-center text-green-500"><i class="fad fa-circle"></i></td>
                <td class="border border-l-0 border-b-0 px-4 py-2">{{ $order->product }}</td>
                <td class="border border-l-0 border-b-0 px-4 py-2">${{ $order->amount }}</span></td>
                <td class="border border-l-0 border-b-0 border-r-0 px-4 py-2">{{ $order->client->name }} {{ $order->client->lastname }}</td>
            </tr>
        @endforeach
      </tbody>
    </table>

</div>
<!-- end Ordenes -->