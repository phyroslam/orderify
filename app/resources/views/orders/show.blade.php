@extends('layout')

@section('content')

<h1 class="h2">{{ $client->name }} {{ $client->lastname}}</h1>
<p class="mt-3 h6">{{ $client->email }} - {{ $client->phone }}</p>

<hr class="my-5">

<h3 class="h5">Ordenes</h3>
<p class="mt-3">Ordenes realizadas</p>

<!-- Ordenes -->
<div class="card">
    
    <div class="card-header">Ordenes</div>

    @foreach ($orders as $order) 
    <div class="p-6 flex flex-row justify-between items-center text-gray-600 border-b">
        <div class="flex items-center"> 
            <a href="{{ route('cliente', ['id' => $client->id ]); }}"><h1>{{ $order->product }}</h1></a>
        </div> 
    </div>
    @endforeach

</div>
<!-- end Ordenes -->
@endsection