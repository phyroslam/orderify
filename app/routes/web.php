<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index']);

//Rutas para clientes
Route::get('/clientes', [ClientsController::class, 'index'])->name('clientes');

Route::post('/clientes/crear', [ClientsController::class, 'create'])->name('clientes.crear');

Route::get('/cliente/{id}', [ClientsController::class, 'show'])->name('cliente');

//Rutas para ordenes
Route::get('/ordenes', [OrdersController::class, 'index'])->name('ordenes');

Route::post('/ordenes/crear', [OrdersController::class, 'create'])->name('ordenes.crear');

Route::get('/orden/{id}', [OrdersController::class, 'show'])->name('orden');
