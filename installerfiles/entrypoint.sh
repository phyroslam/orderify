#!/usr/bin/env bash

#docker-php-ext-install zip pcntl

composer install -d /var/www/html

chown -R www-data:www-data /var/www/html
chmod -R 776 /var/www/html/storage

rm /var/www/html/storage/logs/laravel.log

php /var/www/html/artisan cache:clear

sleep 10

php /var/www/html/artisan migrate
php /var/www/html/artisan cache:clear

apache2-foreground